package mypackage;

import java.io.File;
import java.io.IOException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.InvalidXMLException;

public class AnalysisEngineInitiator {

	private AnalysisEngine analysisEngine = null;
	public AnalysisEngineInitiator() throws ResourceInitializationException{
		File descriptorFile = new File("descriptors/analysisEngine.xml");
		XMLInputSource descriptorSource = null;
		try
		{
			descriptorSource = new XMLInputSource(descriptorFile);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		ResourceSpecifier resourceSpecifier = null;
		try
		{
			resourceSpecifier = UIMAFramework.getXMLParser().parseResourceSpecifier(descriptorSource);
		}
		catch(InvalidXMLException e) {
			e.printStackTrace();
		}
		System.out.println("Error identifier");
		System.out.println(resourceSpecifier);
		analysisEngine = UIMAFramework.produceAnalysisEngine(resourceSpecifier);
		System.out.println("I guess error above this");
	}
	
	public JCas process(String text) throws UIMAException{
		JCas jCas = analysisEngine.newJCas();
		jCas.setDocumentText(text);
		analysisEngine.process(jCas);
		return jCas;
	}
}
