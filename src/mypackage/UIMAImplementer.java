package mypackage;

import java.io.*;
import mypackage.AnalysisEngineInitiator;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.jcas.JCas;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UIMAException;
import org.apache.uima.resource.ResourceInitializationException;


public class UIMAImplementer 
{
	public static void main(String args[])
	{
		AnalysisEngine analysisEngine = null;
		AnalysisEngineInitiator uimaPipeline = null;
		String sampleText = "This is dummy text and email@test.com";
		try 
		{
			uimaPipeline = new AnalysisEngineInitiator();
		}
		catch(ResourceInitializationException rie)
		{
			rie.printStackTrace();
		}
		JCas output = null;
	    try {
	       output = uimaPipeline.process(sampleText);
	    } catch (UIMAException e) {
	        e.printStackTrace();
	    }
	    System.out.println(output.getDocumentAnnotationFs());
		
	}
}
